"""
Course API Serializers.  Representing course catalog data
"""


from django.core.urlresolvers import reverse
from rest_framework import serializers

from openedx.core.djangoapps.models.course_details import CourseDetails
from openedx.core.lib.api.fields import AbsoluteURLField


class EnrolledLearnerSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for Enrollment objects providing all data about the user .
    """
    user_id = serializers.IntegerField()
    username = serializers.CharField()  # pylint: disable=invalid-name
    email = serializers.CharField()
    is_active = serializers.IntegerField()
    mode = serializers.CharField()


class GradeCutoffsSerializer(serializers.Serializer):
    '''
    Serializer for Course Grade Cutoffs
    '''
    label = serializers.CharField()
    grade_cutoff = serializers.FloatField()


class SectionScoresSerializerHeader(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for getting minimal information about the Graded Section.
    """

    name = serializers.CharField()
    maxmarks = serializers.FloatField()
    due = serializers.CharField()
    sec_format = serializers.CharField()
    location_id = serializers.CharField()
    short_label = serializers.CharField()
    problems_maximum_marks = serializers.ListField(
        child=serializers.FloatField()
    )


class SectionScoresSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for user location grade details.
    """

    user_id = serializers.CharField()  # pylint: disable=invalid-name
    location_id = serializers.CharField()
    total_score = serializers.CharField()
    section_scores = serializers.ListField(
        child=serializers.CharField()
    )


class StudentGradesSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for getting Students Grades
    """
    user_id = serializers.CharField()  # pylint: disable=invalid-name
    per_cent = serializers.CharField()
    letter_grade = serializers.CharField()
    sectionwise_marks = serializers.ListField(
        child=serializers.CharField()
    )
