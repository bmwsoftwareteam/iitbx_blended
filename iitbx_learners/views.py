"""
IITBX Learners API Views
"""
from django.contrib.auth.models import User, AnonymousUser
from django.core.exceptions import ValidationError
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.exceptions import PermissionDenied
from rest_framework import authentication, permissions, status, views

from rest_framework_oauth.authentication import OAuth2Authentication

#from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated ,IsAdminUser#,TokenHasReadWriteScope
from openedx.core.lib.api.paginators import NamespacedPageNumberPagination
from openedx.core.lib.api.view_utils import view_auth_classes, DeveloperErrorViewMixin
from .forms import CourseDetailGetForm, CourseListGetForm
from .api import enrollmentlist, get_grade_criteria, get_graded_section_details, get_marks_for_section, get_grade_scores
from .serializers import EnrolledLearnerSerializer, GradeCutoffsSerializer, SectionScoresSerializerHeader, SectionScoresSerializer, StudentGradesSerializer
from opaque_keys import InvalidKeyError
from opaque_keys.edx.keys import CourseKey

from rest_framework.response import Response
from lms.djangoapps.courseware.courses import get_course
from student.models import CourseEnrollment
from courseware.access import has_access


class LearnersListView(DeveloperErrorViewMixin, ListAPIView):
    """
    **Use Cases**

        Request Enrollment Lists for a course

    **Example Requests**

        GET /api/iitbx_learners/v0/learners_list/{course_key}/

    **Response Values**

        Body consists of the list of users of the course  of following fields:
        user_id: Openedx id of the user
        username: Public Username of the user
        email: Email address of the user
        is_active: true or false (true mean as of current enrolled user, false means unenrolled user)

    **Parameters:**
        None

    **Returns**

        * 200 on success with above fields.
        * 400 if an invalid parameter was sent or the course_id was not provided
          for an authenticated request.
        * 403 if a user who does not have permission to masquerade as
          another user specifies a username other than their own.
        * 404 if the course is not available or cannot be seen.

        Example response:
            [
              {  "user_id": 1
                 "username": "admin",
                 "email": "admin@example.com",
                 "is_active": true,
                 "mode": "audit"
              }
               ...
             ]


    """
    pagination_class = NamespacedPageNumberPagination
    serializer_class = EnrolledLearnerSerializer
    authentication_classes = (OAuth2Authentication,)
    permission_classes =(IsAuthenticated,IsAdminUser,)
    def get_queryset(self):

        """
        Return the requested course object, if the user has appropriate
        permissions.
        """
        requested_params = self.request.query_params.copy()
        requested_params.update({'course_key': self.kwargs['course_key_string']})      
        try: 
            userid=str(self.kwargs['userid'])
        except Exception as e:
            raise ('Initial Userid is invalid. Error: '+ str(e.message))
        try:
            numrec=str(self.kwargs['numrec'])
            if int(numrec) <=0:
                numrec =50

        except Exception as e:
            raise ('Initial Number of Records is invalid. Error: '+ str(e.message))
         
        form = CourseDetailGetForm(requested_params, initial={'requesting_user': self.request.user})
        if not form.is_valid():
            raise ValidationError(form.errors)
        course_key = form.cleaned_data['course_key']
        course = get_course(course_key, depth=2)


        return enrollmentlist(form.cleaned_data['course_key'],userid,numrec)


class CourseGradingCriteria(DeveloperErrorViewMixin, ListAPIView):
    """
    **Use Case**

        Get the course grading criteria.

    **Example requests**:

        GET /api/iitbx_learners/v0/course_grade_criteria/{course_id}/

    **Response Values**

        * label: The label for course grade like Pass or Fail or A+,A,B...

        * grade_cutoff: The cutoff for which grade is assigned like (.5,.9 etc._
        **Parameters:**
        None

    **Returns**

        * 200 on success with above fields.
        * 400 if an invalid parameter was sent or the course_id was not provided
          for an authenticated request.
        * 403 if a user who does not have permission to masquerade as
          another user specifies a username other than their own.
        * 404 if the course is not available or cannot be seen.

        Example response:
            [
              {
                "grade_cutoff": 0.5,
                "label": "Pass"
              },
            ]
    """
    serializer_class = GradeCutoffsSerializer
    authentication_classes = (OAuth2Authentication,)
    permission_classes =(IsAuthenticated,IsAdminUser,)
 
    allow_empty = True

    def get(self, request, **kwargs):
        requested_params = self.request.query_params.copy()
        requested_params.update({'course_key': self.kwargs['course_key_string']})
        form = CourseDetailGetForm(requested_params, initial={'requesting_user': self.request.user})
        if not form.is_valid():
            raise ValidationError(form.errors)

        course_key = form.cleaned_data['course_key']
        course = get_course(course_key, depth=2)
        return Response(get_grade_criteria(form.cleaned_data['course_key'], course))


class GradeDetails(DeveloperErrorViewMixin, ListAPIView):
    """
    **Use Case**

        Get the course grades details for all the learners.

    **Example requests**:

        GET /api/iitbx_learners/v0/get_grade_details/{course_id}/

    **Response Values**

        * user_id: User Identifier

        * percent: Progress of course in %

        * grade: The grade of the course depending on the progress

        * sectionwise_marks: The list of marks obtained in each section

    **Returns**

        * 200 on success with above fields.
        * 400 if an invalid parameter was sent or the course_id was not provided
          for an authenticated request.
        * 403 if a user who does not have permission to masquerade as
          another user specifies a username other than their own.
        * 404 if the course is not available or cannot be seen.

        Example response:
                       "results": [
                                   {
                                    "user_id": "1",
                                    "per_cent": "0.0",
                                    "letter_grade": null,
                                    "sectionwise_marks": [
                                                          "NA",
                                                          "NA",
                                                          "NA"
                                                         ]
                                     },
                                     ..
                                     ]

    """
    pagination_class = NamespacedPageNumberPagination
    serializer_class = StudentGradesSerializer
    authentication_classes = (OAuth2Authentication,)
    permission_classes =(IsAuthenticated,IsAdminUser,)


    def get_queryset(self):
        requested_params = self.request.query_params.copy()
        requested_params.update({'course_key': self.kwargs['course_key_string']})
        try:
            userid=str(self.kwargs['userid'])
        except Exception as e:
            raise ('Initial Userid is invalid. Error: '+ str(e.message))
        try:
            numrec=str(self.kwargs['numrec'])
            if int(numrec) <=0:
                numrec =50

        except Exception as e:
            raise ('Initial Number of Records is invalid. Error: '+ str(e.message))


        form = CourseDetailGetForm(requested_params, initial={'requesting_user': self.request.user})
        if not form.is_valid():
            raise ValidationError(form.errors)

        course_key = form.cleaned_data['course_key']
        course = get_course(course_key, depth=2)
        return get_grade_scores(course_key, course,userid,numrec)


class GradedSectionDetails(DeveloperErrorViewMixin, ListAPIView):
    """
    **Use Case**

        Get the course graded section details criteria.

    **Example requests**:

        GET /api/iitbx_learners/v0/get_section_details/{course_id}/

    **Response Values**

        * location: The url location of the section

        * name: The name of the section

        * format: The assignment type of the section

        * due: The due date for the graded questions to complete

        * maxmarks: The list of all possible scrore of the graded problems in the order that is visible to learners._

    """
    serializer_class = SectionScoresSerializerHeader
    authentication_classes = (OAuth2Authentication,)
    permission_classes =(IsAuthenticated,IsAdminUser,)

    def get(self, request, **kwargs):
        requested_params = self.request.query_params.copy()
        requested_params.update({'course_key': self.kwargs['course_key_string']})
        form = CourseDetailGetForm(requested_params, initial={'requesting_user': self.request.user})
        if not form.is_valid():
            raise ValidationError(form.errors)

        course_key = form.cleaned_data['course_key']
        course = get_course(course_key, depth=2)
        user = None
        try:
            user = User.objects.get(username=self.request.user)
        except:
            None
        return Response(get_graded_section_details(course_key, course, user))


class CourseSectionScores(DeveloperErrorViewMixin, ListAPIView):
    pagination_class = NamespacedPageNumberPagination
    serializer_class = SectionScoresSerializer
    authentication_classes = (OAuth2Authentication,)
    permission_classes =(IsAuthenticated,IsAdminUser,)


    def get(self, request, **kwargs):

        requested_params = self.request.query_params.copy()
        requested_params.update({'course_key': self.kwargs['course_key_string']})
        try:
            userid=str(self.kwargs['userid'])
        except Exception as e:
            raise ('Initial Userid is invalid. Error: '+ str(e.message))
        try:
            numrec=str(self.kwargs['numrec'])
            if int(numrec) <=0:
                numrec =50

        except Exception as e:
            raise ('Initial Number of Records is invalid. Error: '+ str(e.message))
        

        form = CourseDetailGetForm(requested_params, initial={'requesting_user': self.request.user})
        if not form.is_valid():
            raise ValidationError(form.errors)

        course_key = form.cleaned_data['course_key']
        course = get_course(course_key, depth=2)

        try:
            usage_key_string = kwargs['location']
        except:
            raise ('Usage Key {} is invalid.'.format(usage_key_string))
        return Response(get_marks_for_section(course_key, course, usage_key_string,userid,numrec))
