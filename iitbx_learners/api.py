"""
IITBX Learners API
"""


from openedx.core.djangoapps.content.course_overviews.models import CourseOverview
from openedx.core.lib.exceptions import CourseNotFoundError

from student.models import CourseEnrollment
#import lms.djangoapps.grades.new.course_grade
from lms.djangoapps.grades.new.course_grade import CourseGradeFactory
from courseware.access import has_access


def _is_course_valid(course_key):
    '''
    Reutrn true if course_key is valid else Raise exception
    '''
    try:
        course = CourseOverview.get_from_id(course_key)
        return True
    except CourseOverview.DoesNotExist:
        msg = u"Requested enrollment information for unknown course {course}".format(course=course_id)
        log.warning(msg)
        raise CourseNotFoundError(msg)
    return False


def _get_enrollments(course_key,userid,numrec, is_active="all"):
    '''
       Get the list of learners of the course
    '''
    if (is_active == 'all'):
        enrollments = CourseEnrollment.objects.filter(course_id=course_key,user__id__gt=userid).order_by('user__id')[:numrec]
    elif (is_active == 'true'):
        enrollments = CourseEnrollment.objects.filter(course_id=course_key, is_active=True,user__id__gt=userid).order_by('user__id')[:numrec]
    elif (is_active == 'false'):
        enrollments = CourseEnrollment.objects.filter(course_id=course_key, is_active=False,user__id__gt=userid).order_by('user__id')[:numrec]
    else:
        enrollments = None
    return enrollments


def _valid_location(course, locationid, user):
    '''
       To verify if the locationid provided is valid graded section for the course.
    '''
    course_grade = CourseGradeFactory().create(user, course)
    courseware_summary = course_grade.chapter_grades
    for chapter in courseware_summary:
        if not chapter['display_name'] == "hidden":
            for section in chapter['sections']:

                if section.graded:
                    if str(section.location) == locationid:
                        return True
    return False


def enrollmentlist(course_key,userid,numrec):
    """
    Return a list of users associated with `course_key`.
    The users information is stored is provided in below format
        user_id,username,email,is_active(1 or 0),mode
    if is_active=1 then the user is enrolled else user is not enrolled
    for the course.

    Arguments:
        course_key (CourseKey): Identifies the course of interest

    Return value:
        studentlist  representing the list of students associated with the
                     course
    """
    if _is_course_valid(course_key):

        studentlist = []
        enrollments =_get_enrollments(course_key,userid,numrec,"all") 
        for student in enrollments:
            dictuser = {"user_id": student.user.id, 'username': student.user.username, 'email': student.user.email, 'is_active': student.is_active, 'mode': student.mode}
            studentlist.append(dictuser)
        return studentlist


def _get_grade_criteria(course):
    '''
       Get the list of grade_cutoffs for a course.
    '''
    grade_cutoffs = []
    if course.grade_cutoffs:
        for label in course.grade_cutoffs:
            cutoff_detail = {"label": label, "grade_cutoff": course.grade_cutoffs[label]}
            grade_cutoffs.append(cutoff_detail)
    return grade_cutoffs


def get_grade_criteria(course_key, course):
    if _is_course_valid(course_key):
        return _get_grade_criteria(course)
    else:
        return None


def _get_graded_section_details(course, user):
    '''
       Get the list of graded sections for  a course
    '''
    section_dict = {}
    sec_details = []
    course_grade = CourseGradeFactory().create(user, course)
    structure = course_grade.course_structure
    courseware_summary = course_grade.chapter_grades
    policy = {}
    for assignment_type in course.raw_grader:
        detail_dict = {}
        try:
            detail_dict['label'] = assignment_type['short_label']
        except:
            detail_dict['label'] = None
        detail_dict["count"] = 0
        policy[assignment_type['type']] = detail_dict
    for chapter in courseware_summary:
        if not chapter['display_name'] == "hidden":
            for section in chapter['sections']:

                section_name = None
                section_due = None
                section_maxmarks = None
                section_format = None
                section_label = None
                location_id = None
                maxmarks_list = []

                if section.graded:
                    if section.display_name is not None:
                        section_name = section.display_name
                    else:
                        section_name = ""
                    if section.format is not None:
                        section_format = section.format
                    else:
                        section_format = ""
                    if section.all_total.possible is not None:
                        section_maxmarks = section.all_total.possible
                    else:
                        section_maxmarks = 0.0
                    if section.due is not None:
                        section_duedate = section.due
                    else:
                        section_duedate = None
                    maxmarks_list = []
                    for score in section.scores:
                        score_possible = ("{0:.3n}".format(float(score.possible)))
                        maxmarks_list.append(float(score_possible))
                    location_id = str(section.location)
                    if policy.has_key(section_format):
                        policy[section_format]["count"] = policy[section_format]["count"] + 1
                    else:
                        continue
                    section_dict = {"name": section_name, "maxmarks": section_maxmarks, "due": section.due, "sec_format": section_format, "location_id": location_id, "problems_maximum_marks": maxmarks_list, "short_label": policy[section_format]["label"]+str(policy[section_format]["count"]).zfill(2)}
                    sec_details.append(section_dict)
    return sec_details


def _get_marks_for_section(course, enrollments, locationid):
    '''
       Get the marks per problem for a graded section  for each active learner in a course
    '''
    earned = ""
    student_score_list = []
    for student in enrollments:

        earned = ""
        score_list = []
        course_grade = CourseGradeFactory().create(student.user, course)
        courseware_summary = course_grade.chapter_grades
        for chapter in courseware_summary:
            if not chapter['display_name'] == "hidden":
                for section in chapter['sections']:
                    if section.graded:
                        if str(section.location) == locationid:

                            if section.all_total.attempted is True:
                                earned = str(section.all_total.earned)

                                for score in section.scores:
                                    if score.attempted:
                                        score_earned = ("{0:.3n}".format(float(score.earned)))
                                        score_list.append(str(score_earned))
                                    else:
                                        score_list.append("NA")
                            else:
                                earned = "NA"

                            block_id = str(section.location)
                            score_for_section = {"user_id": student.user.id, "location_id": block_id, "total_score": earned, "section_scores": score_list}
                            student_score_list.append(score_for_section)
                            break

    return student_score_list


def get_graded_section_details(course_key, course, user):
    '''
    Return a list of graded sections  associated with `course_key`.
    The list of sections contains section name, location key, short label and maximum score possible of each problem in the .

    Arguments:
        course_key (CourseKey): Identifies the course of interest

    Return value:
        graded section list  represents the list of graded sections  associated with the course

    '''
    learner = None
    if _is_course_valid(course_key):
        enrollments = _get_enrollments(course_key,0,1000,is_active="true")
        learner_found = False
        for student in enrollments:
            if has_access(student.user, 'staff', course):
                continue
            else:
                learner_found = True
                learner = student.user
                break
        if learner_found is False:
            learner = enrollments[0].user
        return _get_graded_section_details(course, learner)
    else:
        None


def get_marks_for_section(course_key, course, locationid,userid,numrec):
    '''
    Return a list of users associated with `course_key` and location.
    The users information is stored is provided in below format
        user_id,[marks obtained per problem]

    Arguments:
        course_key (CourseKey): Identifies the course of interest
        location(Graded Section Location): Identifies the location of interest
    Return value:
        studentlist  representing the list of students and marks for the section  associated with the course
    '''
    if _is_course_valid(course_key):
        enrollments = _get_enrollments(course_key,userid,numrec,is_active="true")
        if len(enrollments) == 0:
            return []
        if _valid_location(course, locationid, enrollments[0].user):
            return _get_marks_for_section(course, enrollments, locationid)


def _get_grades_details(course, enrollments):
    '''
    Return a list of users with grades  associated with `course_key`.
    The users information is stored is provided in below format
        user_id,grade,list of mark per section

    Arguments:
        course_key (CourseKey): Identifies the course of interest

    Return value:
        studentlist  representing the list of students with grades  associated with the course

    '''
    students_grades_list = []
    for student in enrollments:
        course_grade = CourseGradeFactory().create(student.user, course)
        structure = course_grade.course_structure
        courseware_summary = course_grade.chapter_grades
        grade_summary = course_grade.summary
        sectionwise_marks = []
        student_grade = {}
        for chapter in courseware_summary:
            if not chapter['display_name'] == "hidden":
                for section in chapter['sections']:
                    if section.graded:
                        if section.all_total.attempted is True:
                            sectionwise_marks.append(str(section.all_total.earned))
                        else:
                            sectionwise_marks.append("NA")
        student_grade = {"user_id": student.user.id, "per_cent": str(grade_summary["percent"]), "letter_grade": grade_summary["grade"], "sectionwise_marks": sectionwise_marks}
        students_grades_list.append(student_grade)
    return students_grades_list


def get_grade_scores(course_key, course,userid,numrec):
    '''
     Return a list of users with grades  associated with `course_key`.
    The users information is stored is provided in below format
        user_id,grade,list of mark per section

    Arguments:
        course_key (CourseKey): Identifies the course of interest

    Return value:
        studentlist  representing the list of students with grades  associated
                     with the course

    '''
    if _is_course_valid(course_key):
        enrollments = _get_enrollments(course_key,userid,numrec, is_active="true")
        student_grades_list = _get_grades_details(course, enrollments)
        return student_grades_list
