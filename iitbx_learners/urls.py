"""
IITBX Learners API URLs
"""
from django.conf import settings
from django.conf.urls import patterns, url, include

from .views import LearnersListView, CourseGradingCriteria, GradedSectionDetails, CourseSectionScores, GradeDetails

"""
Following APIs are included
1. Course Level Learners List
2. Course Level Grade Criteria
3. Course Level  List of Graded Sections
4. Graded Section Level Scores of Active Learners
5. Grade Summary of  Active Learners for a course
"""
urlpatterns = patterns(
    '',
    url(r'^v1/learners_list/{course_key}/(?P<userid>[0-9]+|-[0-9])/(?P<numrec>[0-9]+|-[0-9])$'.format(course_key=settings.COURSE_KEY_PATTERN), LearnersListView.as_view(), name="course-learners"),
    url(r'^v0/course_grade_criteria/{}'.format(settings.COURSE_KEY_PATTERN), CourseGradingCriteria.as_view(), name="course_grades_criteria"),
    url(r'^v0/get_graded_sections/{}'.format(settings.COURSE_KEY_PATTERN), GradedSectionDetails.as_view(), name="course-graded-sections"),
    url(r'^v0/get_section_scores/{course_key}/section/(?P<location>.*)/(?P<userid>[0-9]+|-[0-9])/(?P<numrec>[0-9]+|-[0-9])$'.format(course_key=settings.COURSE_KEY_PATTERN), CourseSectionScores.as_view(), name="get_section_details"),
    url(r'^v0/get_grade_details/{course_key}/(?P<userid>[0-9]+|-[0-9])/(?P<numrec>[0-9]+|-[0-9])$'.format(course_key=settings.COURSE_KEY_PATTERN), GradeDetails.as_view(), name="get_grade_details"),
)
